﻿using System.ComponentModel.DataAnnotations;
using CRC.Repository.Enums;
using CRC.Services.ViewModels.Base;

namespace CRC.Services.ViewModels
{
    public class CreateRequestViewModel: BaseViewModel
    {
        //TODO dodaj adnotacje (walidacja)
        public ServersEnum ServerName { get; set; }
        public string ServerAddress { get; set; }
        public PermissionsEnum Permission { get; set; }
        public int UserId { get; set; }       
        public string AdditionalInfo { get; set; }
    }
}
